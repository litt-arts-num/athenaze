<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [                                                          
   <!ENTITY nbsp  "&#160;" >
]>
<!-- XSLT - Generate HTML output for ELLASS project, Athenaze ancient greek manual
* @author AnneGF@CNRS
* @date : 2022-2024
* version: 08/10/2024
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:html="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs tei html" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <xsl:output method="xhtml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>
    <!-- Dark mode : pour sortir une page HTML en mode sombre. Plus reposant pour les yeux -->
    <xsl:param name="darkmode">0</xsl:param>
    
    <!-- Options -->
    <xsl:param name="notes_marginales">0</xsl:param>
    <xsl:param name="notes_infrapaginales">0</xsl:param>
    <xsl:param name="number_sentence">1</xsl:param>
    <xsl:param name="one_sentence_by_line">0</xsl:param>
    <!-- 1 pour les afficher/activer ; 0 pour ne pas les afficher/désactiver -->

    <!-- collection -->
    <xsl:variable name="corpus" select="collection('Athenaze Chapitres TEI/?select=*.xml')"/>

    <!-- variables pour extraction automatique du nom de fichier -->
    <xsl:variable name="base-uri" select="base-uri(.)"/>
    <xsl:variable name="document-uri" select="document-uri(.)"/>
    <xsl:variable name="filename" select="(tokenize($document-uri, '/'))[last()]"/>

    <!-- Premier template : génération de la structure HTML -->
    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html>
</xsl:text>
        <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
            <head>
                <title>
                    <xsl:value-of
                        select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                    <xsl:text> - Version HTML</xsl:text>
                </title>
                <style>
                    <xsl:if test="$darkmode = '1'">
                        <xsl:text>
                    body { background-color: black !important; color: white !important; }
</xsl:text>
                    </xsl:if>
                    h1,
                    h2 {
                        text-align: center;
                        padding: 1em;
                    }
                    header {
                        font-weight: bold;
                        text-align: center;
                        padding-bottom: 3px;
                    }
                    table.page_content {
                        border: 0;
                        border-spacing: 5px;
                        width: 100%;
                        margin: auto;
                    }
                    ul.list,
                    ul.list li  {
                        text-align: left;
                    }
                    ul.list_vocabulary,
                    ul.note {
                        margin: 0;
                    }
                    p.lesson {
                        font-size: x-large;
                        margin: 0;
                    }
                    p.grammar {
                        padding: 1%;
                        font-size: large;
                    }
                    ul.list_vocabulary a {
                        text-decoration: none;
                    }
                    td.top {
                        vertical-align: top;
                    }
                    td.top24 {
                        width: 24%;
                        vertical-align: top;
                    }
                    td.top52 {
                        width: 52%;
                        vertical-align: top;
                    }
                    td.top76 {
                        width: 76%;
                        vertical-align: top;
                    }</style>

            </head>
            <body style="margin-bottom: 15px;" class="col-10 m-auto">
                <!--  *******************************  -->
                <!--  Affichage du titre du chapitre   -->
                <!--  *******************************  -->
                <h1>
                    <xsl:value-of
                        select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                </h1>
                <!--  ***************************************  -->
                <!--  Affichage de la source bibliographique   -->
                <!--  ***************************************  -->
                <p>
                    <xsl:apply-templates select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc"
                    />
                </p>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

    <!-- ATTENTION ce template ne matche jamais rien... Doit-on l'effacer ? -->
    <xsl:template match="text">
        <xsl:apply-templates/>
        <link rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossorigin="anonymous"/>
    </xsl:template>

    <!--  *************************  -->
    <!--  Éléments d'entête ignoré   -->
    <!--  *************************  -->
    <xsl:template match="tei:teiHeader"/>


    <!--  ***********************  -->
    <!--  Diverses mise en formes   -->
    <!--  ***********************  -->
    <xsl:template match="tei:list">
        <ul class="list">
            <xsl:for-each select="tei:item">
                <li>
                    <xsl:apply-templates/>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <xsl:template match="tei:div[@rendition = 'non']"/>
    <xsl:template match="tei:div">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="@target"/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="not(text())">
                    <xsl:value-of select="@target"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </a>
    </xsl:template>
    <xsl:template match="tei:title">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:p">
        <xsl:choose>
            <xsl:when test="count(child::*) = 1 and name(child::*[1]) = 'list'">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <p class="">
                    <xsl:apply-templates/>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:head">
        <header>
            <xsl:apply-templates/>
        </header>
    </xsl:template>
    <xsl:template match="tei:ab">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <!--  *********************************  -->
    <!--  Affichage du titre de la section   -->
    <!--  *********************************  -->
    <xsl:template match="tei:titlePart">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <!--  *********************  -->
    <!--  Affichage des leçons   -->
    <!--  *********************  -->
    <xsl:template name="leçon">
        <div class="lesson">
            <xsl:choose>
                <xsl:when test="$number_sentence = 1">
                    <xsl:for-each select=".//tei:p[1]">
                        <xsl:call-template name="count">
                            <xsl:with-param name="prev">
                                <xsl:text>0</xsl:text>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <!--  ***********************************  -->
    <!--  Affichage numérotation des phrases   -->
    <!--  ***********************************  -->
    <xsl:template name="count">
        <xsl:param name="prev"/>
        <xsl:variable name="quote">
            <xsl:text>" </xsl:text>
        </xsl:variable>

        <p>
            <xsl:analyze-string
                select="normalize-space(replace(string-join(child::text(), ''), '&nbsp; ', ' '))"
                regex="[\.;;]">
                <xsl:matching-substring>
                    <!-- Process the sentence separators (. and ;) -->
                    <xsl:value-of select="."/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <!-- Process the text part -->
                    <xsl:choose>
                        <!-- When sentence end within a quotation, show the sentence number french-way: after the quote and not before -->
                        <xsl:when test="starts-with(., ' »')">
                            <xsl:text> »</xsl:text>
                        </xsl:when>
                        <xsl:when test="starts-with(., $quote)">
                            <xsl:text>" </xsl:text>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test=". = ' »' or . = $quote"/>
                        <xsl:otherwise>
                            <xsl:if test="$one_sentence_by_line = 1">
                                <br/>
                            </xsl:if>
                            <xsl:text> [</xsl:text>
                            <xsl:value-of select="round(position() div 2) + $prev"/>
                            <xsl:text>] </xsl:text>
                            <xsl:choose>
                                <xsl:when test="starts-with(., ' »')">
                                    <span>
                                        <xsl:value-of select="substring-after(., ' »')"/>
                                    </span>
                                </xsl:when>
                                <xsl:when test="starts-with(., $quote)">
                                    <span>
                                        <xsl:value-of select="substring-after(., $quote)"/>
                                    </span>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span>
                                        <xsl:value-of select="."/>
                                    </span>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </p>
        <xsl:variable name="total"
            select="$prev + count(tokenize(string-join(child::text(), ''), '[\.;]')[not(normalize-space(.) = '')])"/>
        <xsl:for-each select="following-sibling::tei:p[1]">
            <xsl:call-template name="count">
                <xsl:with-param name="prev">
                    <xsl:value-of select="$total"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <!--  ***************************************  -->
    <!--  Par défaut, on ne traite pas les notes   -->
    <!--  2 templates dédiés sont prévus pour être
           appelés (call-template) au besoin       -->
    <!--  ***************************************  -->
    <xsl:template match="tei:note"/>

    <!--  *******************************  -->
    <!--  Affichage des notes marginales   -->
    <!--  *******************************  -->
    <xsl:template name="note_marginale">
        <xsl:if test="not($notes_marginales = 0)">
            <xsl:for-each select="tei:note[@ana = '#note_marginale']">
                <ul class="note">
                    <li>
                        <xsl:apply-templates select="tei:term"/>
                        <xsl:text>&nbsp;: </xsl:text>
                        <xsl:apply-templates select="tei:foreign"/>
                    </li>
                </ul>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <!--  ***********************************  -->
    <!--  Affichage des notes infrapaginales   -->
    <!--  ***********************************  -->
    <xsl:template name="note_infrapaginale">
        <xsl:if test="not($notes_infrapaginales = 0)">
            <xsl:for-each select="tei:note[@ana = '#note_infrapaginale']//tei:list//tei:item">
                <ul class="note">
                    <li>
                        <xsl:apply-templates select="tei:term"/>
                        <xsl:text>&nbsp;: </xsl:text>
                        <xsl:apply-templates select="tei:foreign"/>
                    </li>
                </ul>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>


    <!--  **********************************  -->
    <!--  STRUCTURE GENERALE HTLM : TABLEAU   -->
    <!--  **********************************  -->
    <xsl:template name="table" match="tei:TEI//tei:text/tei:body">
        <xsl:for-each select="tei:div[@ana = '#leçon' and not(@rendition = 'non')]">
            <table class="page_content">
                <tr class="top">
                    <xsl:choose>
                        <xsl:when test="not($notes_marginales = 0)">
                            <td class="top top24">
                                <xsl:call-template name="note_marginale"/>
                            </td>
                            <td class="top top52">
                                <xsl:call-template name="leçon"/>
                            </td>
                        </xsl:when>
                        <xsl:otherwise>
                            <td class="top top76">
                                <xsl:call-template name="leçon"/>
                            </td>
                        </xsl:otherwise>
                    </xsl:choose>

                    <td class="top top24">
                        <xsl:call-template name="note_infrapaginale"/>
                    </td>
                </tr>
            </table>
        </xsl:for-each>
    </xsl:template>

    <!--  ***********************************  -->
    <!--  Ne pas afficher les tei:text @rendition = 'non'   -->
    <!--  ***********************************  -->
    <xsl:template match="tei:text[@rendition = 'non']"/>

    <!--  ***********************************  -->
    <!--  Affichage de la fiche civilisation   -->
    <!--  ***********************************  -->
    <xsl:template match="tei:text[@ana = '#civilisation' and not(@rendition = 'non')]">
        <xsl:choose>
            <xsl:when test="not(tei:front/tei:docTitle/tei:titlePart = '')">
                <xsl:apply-templates select="tei:front/tei:docTitle/tei:titlePart"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message>Titre de la section civilisation manquant</xsl:message>
                <h2>Civilisation</h2>
            </xsl:otherwise>
        </xsl:choose>
        <div class="civilisation">
            <xsl:apply-templates select="tei:body/*"/>
        </div>
    </xsl:template>

    <!--  ********************************  -->
    <!--  Affichage de la fiche grammaire   -->
    <!--  ********************************  -->
    <xsl:template match="tei:text[@ana = '#grammaire' and not(@rendition = 'non')]">
        <xsl:choose>
            <xsl:when test="not(tei:front/tei:docTitle/tei:titlePart = '')">
                <xsl:apply-templates select="tei:front/tei:docTitle/tei:titlePart"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message>Titre de la section grammaire manquant</xsl:message>
                <h2>Grammaire</h2>
            </xsl:otherwise>
        </xsl:choose>
        <div class="grammar">
            <xsl:apply-templates select="tei:body/*"/>
        </div>
    </xsl:template>

    <!--  ***********************************  -->
    <!--  Affichage de la fiche exercices      -->
    <!--  ***********************************  -->
    <xsl:template match="tei:text[@ana = '#exercice' and not(@rendition = 'non')]">
        <xsl:apply-templates select="tei:front/tei:docTitle/tei:titlePart"/>
        <xsl:for-each select="tei:body//tei:div[not(@rendition = 'non')]">
            <xsl:apply-templates/>
        </xsl:for-each>
    </xsl:template>


    <!--  *************************  -->
    <!--  Affichage du vocabulaire   -->
    <!--  *************************  -->
    <xsl:template name="vocabulaire"
        match="tei:text[@ana = '#vocabulaire' and not(@rendition = 'non')]">
        <xsl:apply-templates select="tei:front/tei:docTitle/tei:titlePart"/>
        <xsl:for-each select="tei:body/tei:list">
            <xsl:apply-templates select="tei:head"/>
            <ul class="list_vocabulary">
                <xsl:for-each select="tei:item">
                    <li>
                        <xsl:choose>
                            <xsl:when test="tei:term/@target and not(tei:term/@target = '...')">
                                <a>
                                    <xsl:attribute name="href" select="tei:term/@target"/>
                                    <xsl:apply-templates mode="vocabulary"/>
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates mode="vocabulary"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </li>
                </xsl:for-each>
            </ul>
        </xsl:for-each>
    </xsl:template>

    <xsl:template mode="#all" match="tei:term">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template mode="#all" match="tei:foreign">
        <span class="ml-3">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template mode="#all" match="tei:*[@rendition = 'non']"/>
    
</xsl:stylesheet>
