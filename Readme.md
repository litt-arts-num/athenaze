# **Projet déplacé sur https://gricad-gitlab.univ-grenoble-alpes.fr/elan/athenaze**




## Hiérarchie et organisation des dossiers
On conseille l'organisation des dossiers suivante :
```
.
├── Athenaze Chapitres TEI
│   ├── chapitre_1_TEI.xml
│   ├── chapitre_2_TEI.xml
│   ├── chapitre_3_TEI.xml
│   ├── chapitre_4_TEI.xml
│   ├── chapitre_5_TEI.xml
│   ├── chapitre_6_TEI.xml
│   └── chapitre_7_TEI.xml
├── Athenaze.xsl
├── Fiche_modele_TEI.xml
├── html
│   └── chapitre_6_TEI.html
└── Readme.md
```
Les fichiers TEI sont dans le sous-dossier `Athenaze Chapitres TEI/`.
La feuille de transformation XSL (dite XSLT) se situe directement dans le dossier principal.
Les fichiers html générés par la transformation sont dans le sous-dossier `html/`.
Une fiche modèle ainsi que ce Readme sont proposés pour documenter et aider à l'encodage et aux manipulation.


# Créer une version HTML avec Oxygen
## Création du scénario de transformation sous Oxygen
* Onglet XSLT
  * XML URL : `${currentFileURL}`
  * XSL URL : choisir le ficher Athenaze.xsl
* Onglet Sortie :
  * Enregistrer sous : `${cfd}/../html/${cfn}.html`
  * Si besoin, cocher "Ouvrir dans le navigateur"

## Génération de la sortie HTML sous Oxygen
* Lancer le scénario de transformation **depuis un fichier XML**

## Pour créer une version PDF
* "Imprimer en PDF" la version HTML depuis son navigateur

# Créer une version HTML en ligne de commande
`saxonb-xslt -ext:on $FICHIER_XML_SOURCE Athenaze.xsl darkmode=1 >$FICHIER_HTML_SORTIE`

Pour lancer sur tous les chapitres, utiliser le code suivant :
```bash
ls data/*.xml | while read xml_file
do
   out=`echo $xml_file | sed 's#^.*/#html/#; s#\.xml$#.html#'`;
   echo $xml_file" ==> "$out; saxonb-xslt -ext:on $xml_file Athenaze.xsl darkmode=1 >$out;
done
```

# Options d'affichage disponibles
## Affichage des notes marginales
* Au sein du XSLT,  il est possible de renseigner soit 1, soit 0 pour indiquer respectivement si les notes marginales doivent être affichées ou pas.

```xml
<!-- option -->
<xsl:param name="notes_marginales">1</xsl:param>
<!-- 1 pour les afficher ; 0 pour ne pas les afficher -->
```

Pour afficher les notes marginales :
```xml
<xsl:param name="notes_marginales">1</xsl:param>
```

pour ne pas les afficher :

```xml
<xsl:param name="notes_marginales">0</xsl:param>
```

## Autres options sur le même modèle
```xml
    <!-- Dark mode : pour sortir une page HTML en mode sombre. Plus reposant pour les yeux -->
    <xsl:param name="darkmode">0</xsl:param>
    
    <!-- Options -->
    <xsl:param name="notes_marginales">0</xsl:param>
    <xsl:param name="notes_infrapaginales">0</xsl:param>
    <xsl:param name="number_sentence">1</xsl:param>
    <xsl:param name="one_sentence_by_line">0</xsl:param>
    <!-- 1 pour les afficher/activer ; 0 pour ne pas les afficher/désactiver -->
```

# Conseils d'encodage
## Encodage permettant de ne pas afficher une `<div/>`

```xml
<div rendition="non">
  <head>Exercice  XXX</head>
  <p>contenu et énoncé...</p>
</div>
```

Pour finalement afficher l'exercice, il faut soit enlever complètement le couple attribut-valeur, soit changer la valeur (mettre par exemple à "oui" ; actuellement, l'XSL ne fait que vérifier si la valeur est "non", cas dans lequel on n'affiche pas la `<div/>` ; toutes les autres valeurs de cet attribut sont ignorées).

```xml
<div>
  <head>Exercice  XXX</head>
  <p>contenu et énoncé...</p>
</div>
```

ou

```xml
<div rendition="oui">
  <head>Exercice  XXX</head>
  <p>contenu et énoncé...</p>
</div>
```

-----
